# Playbook Windows

This repository provides a number of Playbooks to run routine tasks on Windows. There are respective Job Templates on Red Hat Ansible Automation Platform, if you use the [playbook-rhaap](https://gitlab.com/redhat-cop/ansible-ssa/playbook-rhaap) project. More Details can also be found on the [Ansible Labs](https://www.ansible-labs.de) project page.
## Deploy Server

Run the `windows-server.yml` playbook to create a new Windows Server instance.

## Retrieve Password

To be able to run subsequent tasks, you need the Windows Password. On EC2 the `windows-password.yml` playbook can be used. The playbook requires the private SSH key in PEM format.

```json
{
  "ec2_key_pair": "-----BEGIN RSA PRIVATE KEY----- removed data -----END RSA PRIVATE KEY-----"
}
```

You can use the following command to convert your SSH private key to PEM format:

**NOTE:** This will override the given file! Make a copy first!

```bash
ssh-keygen -f id_rsa -m pem -p
```

## Install base packages

This will install some basic Windows packages by using Chocolatey on the server.
